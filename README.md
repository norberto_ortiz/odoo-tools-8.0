Odoo 8 tools by [odoo-solution.ch](http://www.odoo-solution.ch)
=============

account_followup_custom
-------------

**Swiss compatible followup/overdue module**

Default follow up workflow will be enhanced with functions to manually change the followup level for single move lines.
In followup level configuration, there is a additional field for fees, if you want add a fee for a specific followup level. Remember, if customer pays open invoices and the followup fee, you have to manually create a move line for the fee itself.

***

account_invoice_rounding
-------------

**Create invoices based on default currency rounding factor or with special rounding factor**

* INFO: fix for register payment is already integrated (account_voucher_payment_fix)
* WARNING: don't install along with account_voucher_payment_fix

On invoice level you can change the rounding factor for this invoice only.
If company currency rounding factor is eg. 0.05, you are able to change the rounding factor for a specific invoice to 0.01.

***

account_voucher_payment_fix
-------------

**Fix for github bug 1892: [Difference Amount in "Register Payment" is not displayed properly](https://github.com/odoo/odoo/issues/1892)**

**Fix for lp bug 1202127: [Can't able to pay with write-off via register payment button.](https://bugs.launchpad.net/openobject-addons/+bug/1202127)**

**Fix for github bug 6251: [Sale/Purchase Receipt wrong period on journal change](https://github.com/odoo/odoo/issues/6251)**


* INFO: does fix problem of unbalanced move lines on customer/purchase receipts which contains taxes
* WARNING: don't install along with account_invoice_rounding

***

hr_payslip_extension
-------------

**Swiss payroll customization**

Default Odoo payroll methods are enhanced with some Swiss specific functions.
Additional, Swiss specific salary rules are added.
A simple timesheet is used to easy capture working times of employee.

***

hr_payslip_extension_new
-------------

**Swiss payroll customization (Development)**

* Development of hr_payslip_extension
* will be merged into hr_payslip_extension if development is finished

***

hr_payslip_extension_report
-------------

**Swiss payroll allowance and deduction overview report**

***

l10n_ch_account_vat_payment (testing)
-------------

**Account VAT based on invoice full and partial payments to create VAT report based on payments**

**Configuration**

This module needs a special VAT suspend account, where invoice taxes will be temporary saved after invoice validation. If invoice payment is registered, taxes will be moved from VAT suspend account to default VAT account.

The VAT suspend account does hold all open taxes where invoice isn't paid.

**Information**

In directory scripts, you will find a script (pau_script_v8) to update all invoices to fit this module. The script will cancel invoices (reconcile move lines, remove payment), validate and register payment, if invoice was paid before.

This module also contains a new VAT report, which is based on payments. You will find it at same place where the default VAT report is listed.

***

l10n_ch_base_bank
-------------

**Types and number validation for swiss electronic pmnt. DTA, ESR**

***

l10n_ch_payment_slip
-------------

**Print ESR/BVR payment slip with your invoices**

***

l10n_ch_zip
-------------

**Swiss ZIP/City data**

***
