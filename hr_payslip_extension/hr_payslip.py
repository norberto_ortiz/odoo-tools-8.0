# -*- coding: utf-8 -*-
##############################################################################
#
#    Tech-Receptives Solutions Pvt. Ltd.
#    (C) Copyright 2013 Tech-Receptives Solutions (P) Ltd.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import time

from openerp import netsvc
from openerp.osv import fields, osv
from openerp.tools.translate import _
from datetime import datetime, timedelta
import math
import openerp.addons.decimal_precision as dp
import time
from datetime import date
from datetime import datetime
from datetime import timedelta
from dateutil import relativedelta
import calendar
from openerp.tools.float_utils import float_round
import openerp.addons.decimal_precision as dp
from bsddb.dbtables import _columns

def _employee_get(obj, cr, uid, context=None):
    ids = []
    if context is None:
        context = {}
    if 'active_ids' in context:
        ids = context['active_ids']
    else:
        ids = obj.pool.get('hr.employee').search(cr, uid, [('user_id', '=', uid)], context=context)
    return ids and ids[0] or False

class hr_expense_expense(osv.osv):
    
    _inherit = 'hr.expense.expense'
    
    
    _defaults ={
                'employee_id': _employee_get,
                }
    
class hr_expense_line(osv.osv):
    _inherit = "hr.expense.line"
    
    _columns = {
                'product_id': fields.many2one('product.product', 'Product', domain=[('hr_expense_ok','=',True)], required=True)
                }


class hr_hour_rate_line(osv.osv):
    _name = 'hr.hour.rate.line'
    _rec_name = 'hourly_rate'
    
    _columns = {
        'seq_number': fields.char(size=16, string='Name'),
        'description': fields.char(string='Description'),
        'hourly_rate':fields.float("Hour Rate" ,digits_compute=dp.get_precision('HR')),
        'contract_id':fields.many2one('hr.contract',"Contract")
    }
    
    
    def unlink(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        timesheet_emp_obj = self.pool.get('hr.my.timesheet.employee')
        for hrl in self.browse(cr, uid, ids, context=context):
            timesheet_ids = timesheet_emp_obj.search(cr, uid, [('my_timesheet_id.state','=','done'),('hr_hour_rate_ids','=',hrl.id)], context=context)
            if timesheet_ids:
                raise osv.except_osv(
                    ('Invalid Action!'), 
                    ('In order to delete a hourly rate record, you must first cancel/delete related Timesheet line!.')
                )
        return super(hr_hour_rate_line,self).unlink(cr, uid, ids,context=context)
        
    

    
class hr_contract(osv.osv):
    
    _inherit = "hr.contract"
   
#     def _calculate_basic(self, cr, uid, ids, field_name, arg, context):
#         res = {}
#         for self_obj in self.browse(cr, uid, ids, context=context):
#             total = 0.0
#             for line in self_obj.my_timesheet_line:
#                 total += line.present_hours * line.hr_hour_rate_ids.hourly_rate
#             res[self_obj.id] = total
#         return res
   
    def _check_contract(self, cr, uid, ids,context=None):
        for self_obj in self.browse(cr, uid, ids, context=context):
            self_search_end_date = self.search(cr, uid, [('employee_id','=',self_obj.employee_id.id),('date_start','<=',self_obj.date_start),('date_end','>=',self_obj.date_start)])
            self_search_end_date = list(set(self_search_end_date)-set(ids))
            self_search = self.search(cr, uid, [('employee_id','=',self_obj.employee_id.id),('date_end','=',False)])
            self_search = list(set(self_search)-set(ids))
            if self_search or self_search_end_date:
                return False
        return True
    
    
    _constraints = [
        (_check_contract, 'contract is open for same employee', ['employee_id','date_start','date_end']),
    ]
    
    _columns = {
        'month_hours': fields.float('Month Hours', digits_compute=dp.get_precision('HR')),
        'hour_rate': fields.float('Hour Rate', digits_compute=dp.get_precision('HR')),
        'bvg': fields.float('BVG Deduction', digits_compute=dp.get_precision('HR')),
        'fix_exp': fields.float('Fixed Expense', digits_compute=dp.get_precision('HR')),
        'withholding_tax': fields.float('Withholding Tax', digits_compute=dp.get_precision('HR')),
        'month_wage_13': fields.float('13. Month Wage', digits_compute=dp.get_precision('HR')),
        'month_wage_14': fields.float('14. Month Wage', digits_compute=dp.get_precision('HR')),
        'child_exp': fields.float('Child Expense(<16)', digits_compute=dp.get_precision('HR')),
        'child_exp2': fields.float('Child Expense(>=16)', digits_compute=dp.get_precision('HR')),
        'hour_rate_line_ids': fields.one2many('hr.hour.rate.line', 'contract_id', 'Hour Rate'),
        'my_timesheet_line': fields.one2many('hr.my.timesheet.employee', 'contract_id', 'Salary Hourly Rate'),
        'data_input_line_ids': fields.one2many('custom.hr.payslip.input', 'contract_id', 'Payslip Inputs', required=False, copy=False),
    }
    
    _defaults = {
        'month_hours': 0.0,
        'hour_rate': 0.0,
        'child_exp': 200.0,
        'child_exp2': 250.0,
            }
    
    
class hr_my_timesheet(osv.osv):
    
    _name = "hr.my.timesheet"
    
    _columns = {
                'name': fields.char('Name', size=256, required=True),
                'date_from':fields.date(string="From Date",required=True),
                'date_to':fields.date(string="To Date",required=True),
                'timesheet_employee_line':fields.one2many('hr.my.timesheet.employee','my_timesheet_id',string="Employee Timesheet", copy=True, required=True),
                'state':fields.selection([('draft','Draft'),('done','Done'),('cancel','Cancel')],"States"),
                }
    
    _defaults = {
                  'state':'draft',
                  'date_from': lambda *a: time.strftime('%Y-%m-01'),
                  'date_to': lambda *a: str(datetime.now() + relativedelta.relativedelta(months=+1, day=1, days=-1))[:10],
                 }
                 
    _order = 'date_from desc'
    
    def copy(self, cr, uid, id, default=None, context=None):
        if default is None:
            default = {}
        timesheet = self.browse(cr, uid, id, context=context)
        default['name'] = _("%s (copy)") % (timesheet['name'])
        return super(hr_my_timesheet, self).copy(cr, uid, id, default=default, context=context)
    
    def get_timesheet_confirm(self, cr, uid, ids, context={}):
        for self_obj in self.browse(cr, uid, ids, context=context):
            self.write(cr, uid, [self_obj.id], {'state': 'done'})
        return True
    
    def get_timesheet_cancel(self, cr, uid, ids, context={}):
        for self_obj in self.browse(cr, uid, ids, context=context):
            self.write(cr, uid, [self_obj.id], {'state': 'cancel'})
        return True
    
    def get_timesheet_set_to_draft(self, cr, uid, ids, context={}):
        for self_obj in self.browse(cr, uid, ids, context=context):
            self.write(cr, uid, [self_obj.id], {'state': 'draft'})
        return True
    
    def unlink(self, cr, uid, ids, context=None):
        payslip_obj = self.pool.get('hr.payslip')
        for pay in self.browse(cr, uid, ids, context=context):
            payslip_ids = payslip_obj.search(cr, uid, [('my_timesheet_id','=',pay.id),('state','=','done')], context=context)
            if payslip_ids:
                raise osv.except_osv(
                    ('Invalid Action!'), 
                    ('In order to delete a timesheet, you must first cancel it to delete related Payslip!.')
                )
        return super(hr_my_timesheet, self).unlink(cr, uid, ids, context=context)
        
    
    
class hr_my_timesheet_employee(osv.osv):
    
    _name = "hr.my.timesheet.employee"
    _rec_name='employee_id'
    _order = 'date_from'
    
    def onchange_employee_id(self, cr, uid, ids, employee_id, context=None):
        res = {'value':{'contract_id':False}}
        employee_obj = self.pool.get('hr.employee')
        contract_obj = self.pool.get('hr.contract')
        if employee_id:
            contract_id = contract_obj.search(cr, uid, [('employee_id','=',employee_id)], context=context)
            res['value'] = {
                            'contract_id': contract_id and contract_id[0] or False,
                            }
        return res
    
    def _calculate_basic(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for line in self.browse(cr, uid, ids, context=context):
            total = 0.0
            total += line.present_hours * line.hr_hour_rate_ids.hourly_rate
            res[line.id] = total
        return res
    
    _columns = {
                'employee_id':fields.many2one('hr.employee',string="Employee"),
                'present_days': fields.float('Present Day', digits_compute=dp.get_precision('HR')),
                'present_hours': fields.float('Present Hours', digits_compute=dp.get_precision('HR')),
                'my_timesheet_id':fields.many2one('hr.my.timesheet',string="Timesheet"),
                'date_from':fields.related('my_timesheet_id', 'date_from',type="date",string="From Date",store=True),
                'date_to':fields.related('my_timesheet_id','date_to',type="date",string="To Date",store=True),
                'hr_hour_rate_ids':fields.many2one('hr.hour.rate.line',string="Hourly Rate"),
                'contract_id':fields.many2one('hr.contract',string="Contract"),
                'total_basic_salary': fields.function(_calculate_basic, method=True, type='float', string='Total', digits_compute=dp.get_precision('Payroll')),
                }
    
    
class hr_payslip_worked_days(osv.osv):
    _inherit = 'hr.payslip.worked_days'

    _columns = {
        'working_hours': fields.float('Working Hours'),
    }
    
    _defaults = {
        'working_hours': 0.0
    }

class hr_payslip(osv.osv):
    _inherit = "hr.payslip"
    
    def _net_days(self, cr, uid, ids, field_name, arg, context={}):
        res = {}.fromkeys(ids, 0.0)
        for self_obj in self.browse(cr, uid, ids, context=context):
            total = 0.0
            for line in self_obj.worked_days_line_ids:
                total += line.number_of_days
            res[self_obj.id] = total
        return res
    
    def _net_hours(self, cr, uid, ids, field_name, arg, context={}):
        res = {}.fromkeys(ids, 0.0)
        for self_obj in self.browse(cr, uid, ids, context=context):
            total = 0.0
            for line in self_obj.worked_days_line_ids:
                total += line.working_hours
            res[self_obj.id] = total
        return res
    
    def _get_expense(self, cr, uid, ids, field_name, args=[], context={}):
        res = {}.fromkeys(ids, 0.0)
        exp_obj = self.pool.get('hr.expense.expense')
        for obj in self.browse(cr, uid, ids):
            total = 0.0
            ex_ids = exp_obj.search(cr, uid, [('employee_id', '=', obj.employee_id.id), ('date', '>=', obj.date_from),
                                ('date', '<=', obj.date_to), ('state', '=', 'done')])
            for line in exp_obj.browse(cr, uid, ex_ids):
                total += line.amount
            res[obj.id] = total
        return res
    
    def _get_child_under_16_exp(self, cr, uid, ids, field_name, args=[], context={}):
        res = {}.fromkeys(ids, 0.0)
        exp_obj = self.pool.get('hr.expense.expense')
        for obj in self.browse(cr, uid, ids):
            total = 0.0
            for child in obj.employee_id.child_ids:
                birthday_month = (datetime.strptime(child.dob, '%Y-%m-%d')).strftime('%B')
                payslip_month = (datetime.strptime(obj.date_to, '%Y-%m-%d')).strftime('%B')
                if ((datetime.strptime(obj.date_to, '%Y-%m-%d') - datetime.strptime(child.dob, '%Y-%m-%d')).days / 365) <= 16 and birthday_month == payslip_month:
                    total += 1
                elif ((datetime.strptime(obj.date_to, '%Y-%m-%d') - datetime.strptime(child.dob, '%Y-%m-%d')).days / 365) < 16:
                    total += 1
            res[obj.id] = total
        return res

    def _get_child_over_or_equal_16_exp(self, cr, uid, ids, field_name, args=[], context={}):
        res = {}.fromkeys(ids, 0.0)
        exp_obj = self.pool.get('hr.expense.expense')
        for obj in self.browse(cr, uid, ids):
            total = 0.0
            for child in obj.employee_id.child_ids:
                birthday_month = (datetime.strptime(child.dob, '%Y-%m-%d')).strftime('%B')
                payslip_month = (datetime.strptime(obj.date_to, '%Y-%m-%d')).strftime('%B')
                if ((datetime.strptime(obj.date_to, '%Y-%m-%d') - datetime.strptime(child.dob, '%Y-%m-%d')).days / 365) > 16:
                    total += 1
                elif ((datetime.strptime(obj.date_to, '%Y-%m-%d') - datetime.strptime(child.dob, '%Y-%m-%d')).days / 365) == 16 and birthday_month != payslip_month:
                    total += 1
            res[obj.id] = total
        return res
    
    
    def get_payslip_lines(self, cr, uid, contract_ids, payslip_id, context):
        def _sum_salary_rule_category(localdict, category, amount):
            if category.parent_id:
                localdict = _sum_salary_rule_category(localdict, category.parent_id, amount)
            localdict['categories'].dict[category.code] = category.code in localdict['categories'].dict and localdict['categories'].dict[category.code] + amount or amount
            return localdict

        class BrowsableObject(object):
            def __init__(self, pool, cr, uid, employee_id, dict):
                self.pool = pool
                self.cr = cr
                self.uid = uid
                self.employee_id = employee_id
                self.dict = dict

            def __getattr__(self, attr):
                return attr in self.dict and self.dict.__getitem__(attr) or 0.0

        class InputLine(BrowsableObject):
            """a class that will be used into the python code, mainly for usability purposes"""
            def sum(self, code, from_date, to_date=None):
                if to_date is None:
                    to_date = datetime.now().strftime('%Y-%m-%d')
                result = 0.0
                self.cr.execute("SELECT sum(amount) as sum\
                            FROM hr_payslip as hp, hr_payslip_input as pi \
                            WHERE hp.employee_id = %s AND hp.state = 'done' \
                            AND hp.date_from >= %s AND hp.date_to <= %s AND hp.id = pi.payslip_id AND pi.code = %s",
                           (self.employee_id, from_date, to_date, code))
                res = self.cr.fetchone()[0]
                return res or 0.0

        class WorkedDays(BrowsableObject):
            """a class that will be used into the python code, mainly for usability purposes"""
            def _sum(self, code, from_date, to_date=None):
                if to_date is None:
                    to_date = datetime.now().strftime('%Y-%m-%d')
                result = 0.0
                self.cr.execute("SELECT sum(number_of_days) as number_of_days, sum(number_of_hours) as number_of_hours\
                            FROM hr_payslip as hp, hr_payslip_worked_days as pi \
                            WHERE hp.employee_id = %s AND hp.state = 'done'\
                            AND hp.date_from >= %s AND hp.date_to <= %s AND hp.id = pi.payslip_id AND pi.code = %s",
                           (self.employee_id, from_date, to_date, code))
                return self.cr.fetchone()

            def sum(self, code, from_date, to_date=None):
                res = self._sum(code, from_date, to_date)
                return res and res[0] or 0.0

            def sum_hours(self, code, from_date, to_date=None):
                res = self._sum(code, from_date, to_date)
                return res and res[1] or 0.0

        class Payslips(BrowsableObject):
            """a class that will be used into the python code, mainly for usability purposes"""

            def sum(self, code, from_date, to_date=None):
                if to_date is None:
                    to_date = datetime.now().strftime('%Y-%m-%d')
                self.cr.execute("SELECT sum(case when hp.credit_note = False then (pl.total) else (-pl.total) end)\
                            FROM hr_payslip as hp, hr_payslip_line as pl \
                            WHERE hp.employee_id = %s AND hp.state = 'done' \
                            AND hp.date_from >= %s AND hp.date_to <= %s AND hp.id = pl.slip_id AND pl.code = %s",
                            (self.employee_id, from_date, to_date, code))
                res = self.cr.fetchone()
                return res and res[0] or 0.0

        #we keep a dict with the result because a value can be overwritten by another rule with the same code
        result_dict = {}
        rules = {}
        categories_dict = {}
        blacklist = []
        payslip_obj = self.pool.get('hr.payslip')
        inputs_obj = self.pool.get('hr.payslip.worked_days')
        obj_rule = self.pool.get('hr.salary.rule')
        payslip = payslip_obj.browse(cr, uid, payslip_id, context=context)
        worked_days = {}
        for worked_days_line in payslip.worked_days_line_ids:
            worked_days[worked_days_line.code] = worked_days_line
        inputs = {}
        for input_line in payslip.input_line_ids:
            inputs[input_line.code] = input_line
        
        new_inputs = {}
        new_input_amount = {}
        new_input_rules = []
        
        for new_input_line in payslip.input_line_ids:
            new_inputs.update({new_input_line.salary_rule_id.id : new_input_line.name})
            new_input_amount.update({new_input_line.salary_rule_id.id : new_input_line.salary_rule_id.company_id.currency_id.round(new_input_line.amount)})
            new_input_rules.append((new_input_line.salary_rule_id.id, new_input_line.salary_rule_id.sequence))

        categories_obj = BrowsableObject(self.pool, cr, uid, payslip.employee_id.id, categories_dict)
        input_obj = InputLine(self.pool, cr, uid, payslip.employee_id.id, inputs)
        worked_days_obj = WorkedDays(self.pool, cr, uid, payslip.employee_id.id, worked_days)
        payslip_obj = Payslips(self.pool, cr, uid, payslip.employee_id.id, payslip)
        rules_obj = BrowsableObject(self.pool, cr, uid, payslip.employee_id.id, rules)

        baselocaldict = {'categories': categories_obj, 'rules': rules_obj, 'payslip': payslip_obj, 'worked_days': worked_days_obj, 'inputs': input_obj}
        #get the ids of the structures on the contracts and their parent id as well
        structure_ids = self.pool.get('hr.contract').get_all_structures(cr, uid, contract_ids, context=context)
        #get the rules of the structure and thier children
        rule_ids = self.pool.get('hr.payroll.structure').get_all_rules(cr, uid, structure_ids, context=context)
        rule_ids = rule_ids + new_input_rules
        #run the rules by sequence
        sorted_rule_ids = [id for id, sequence in sorted(rule_ids, key=lambda x:x[1])]
        for contract in self.pool.get('hr.contract').browse(cr, uid, contract_ids, context=context):
            employee = contract.employee_id
            localdict = dict(baselocaldict, employee=employee, contract=contract)
            for rule in obj_rule.browse(cr, uid, sorted_rule_ids, context=context):
                key = rule.code + '-' + str(contract.id)
                localdict['result'] = None
                localdict['result_qty'] = 1.0
                localdict['result_rate'] = 100
                #check if the rule can be applied
                if obj_rule.satisfy_condition(cr, uid, rule.id, localdict, context=context) and rule.id not in blacklist:
                    #compute the amount of the rule
                    amount, qty, rate = obj_rule.compute_rule(cr, uid, rule.id, localdict, context=context)
                    #check if there is already a rule computed with that code
                    previous_amount = rule.code in localdict and localdict[rule.code] or 0.0
                    #set/overwrite the amount computed for this rule in the localdict
                    if amount == 0.0:
                        tot_rule = new_input_amount.get(rule.id, False) and new_input_amount[rule.id] or 0.0
                    else:
                        tot_rule = rule.company_id.currency_id.round((amount * qty * rate) / 100.0)
                    localdict[rule.code] = tot_rule
                    rules[rule.code] = rule
                    #sum the amount for its salary category
                    localdict = _sum_salary_rule_category(localdict, rule.category_id, tot_rule - previous_amount)
                    #create/overwrite the rule in the temporary results
                    if payslip.child_exps_over_16 > 0 and not rule.child_age_selection == 'under_16':
                        result_dict[key] = {
                            'salary_rule_id': rule.id,
                            'contract_id': contract.id,
                            'name': new_inputs.get(rule.id, False) and new_inputs[rule.id] or rule.name,
                            'code': rule.code,
                            'category_id': rule.category_id.id,
                            'sequence': rule.sequence,
                            'appears_on_payslip': rule.appears_on_payslip,
                            'condition_select': rule.condition_select,
                            'condition_python': rule.condition_python,
                            'condition_range': rule.condition_range,
                            'condition_range_min': rule.condition_range_min,
                            'condition_range_max': rule.condition_range_max,
                            'amount_select': rule.amount_select,
                            'amount_fix': rule.amount_fix,
                            'amount_python_compute': rule.amount_python_compute,
                            'amount_percentage': rule.amount_percentage,
                            'amount_percentage_base': rule.amount_percentage_base,
                            'register_id': rule.register_id.id,
                            'amount': new_input_amount.get(rule.id, False) and new_input_amount[rule.id] or amount,
                            'employee_id': contract.employee_id.id,
                            'quantity': float(eval(rule.quantity)) or qty,
                            'rate': rate,
                        }
                    elif payslip.child_exps_under_16 > 0 and not rule.child_age_selection == 'over_16':
                        result_dict[key] = {
                            'salary_rule_id': rule.id,
                            'contract_id': contract.id,
                            'name': new_inputs.get(rule.id, False) and new_inputs[rule.id] or rule.name,
                            'code': rule.code,
                            'category_id': rule.category_id.id,
                            'sequence': rule.sequence,
                            'appears_on_payslip': rule.appears_on_payslip,
                            'condition_select': rule.condition_select,
                            'condition_python': rule.condition_python,
                            'condition_range': rule.condition_range,
                            'condition_range_min': rule.condition_range_min,
                            'condition_range_max': rule.condition_range_max,
                            'amount_select': rule.amount_select,
                            'amount_fix': rule.amount_fix,
                            'amount_python_compute': rule.amount_python_compute,
                            'amount_percentage': rule.amount_percentage,
                            'amount_percentage_base': rule.amount_percentage_base,
                            'register_id': rule.register_id.id,
                            'amount': new_input_amount.get(rule.id, False) and new_input_amount[rule.id] or amount,
                            'employee_id': contract.employee_id.id,
                            'quantity': float(eval(rule.quantity)) or qty,
                            'rate': rate,
                        }
                    elif not rule.child_age_selection == 'over_16' and not rule.child_age_selection == 'under_16':
                            result_dict[key] = {
                            'salary_rule_id': rule.id,
                            'contract_id': contract.id,
                            'name': new_inputs.get(rule.id, False) and new_inputs[rule.id] or rule.name,
                            'code': rule.code,
                            'category_id': rule.category_id.id,
                            'sequence': rule.sequence,
                            'appears_on_payslip': rule.appears_on_payslip,
                            'condition_select': rule.condition_select,
                            'condition_python': rule.condition_python,
                            'condition_range': rule.condition_range,
                            'condition_range_min': rule.condition_range_min,
                            'condition_range_max': rule.condition_range_max,
                            'amount_select': rule.amount_select,
                            'amount_fix': rule.amount_fix,
                            'amount_python_compute': rule.amount_python_compute,
                            'amount_percentage': rule.amount_percentage,
                            'amount_percentage_base': rule.amount_percentage_base,
                            'register_id': rule.register_id.id,
                            'amount': new_input_amount.get(rule.id, False) and new_input_amount[rule.id] or amount,
                            'employee_id': contract.employee_id.id,
                            'quantity': float(eval(rule.quantity)) or qty,
                            'rate': rate,
                        }
                        
                else:
                    #blacklist this rule and its children
                    blacklist += [id for id, seq in self.pool.get('hr.salary.rule')._recursive_search_of_rules(cr, uid, [rule], context=context)]

        result = [value for code, value in result_dict.items()]
        return result
    
    def _get_timesheet_line(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for time in self.browse(cr, uid, ids, context=context):
            res[time.id] = self.pool.get('hr.my.timesheet.employee').search(cr, uid, 
                [('my_timesheet_id.state','=','done'),('employee_id', '=', time.employee_id.id),('my_timesheet_id.date_from','>=',time.date_from),('my_timesheet_id.date_to','<=',time.date_to)], context=context)
        return res
    
    def _calculate_basic(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for pay in self.browse(cr, uid, ids, context=context):
            res[pay.id] = 0.0
            for line in pay.my_timesheet_emp_line:
                 res[pay.id] += line.total_basic_salary
        return res
        
    
    def _get_journal(self, cr, uid, context=None):
        journal_id = self.pool.get('account.journal').search(cr, uid, [('code', '=', 'CSJ'),('name', '=', 'Custom Salary Journal')])
        return journal_id and journal_id[0] or False
    
    _GET_MONTH_LIST = [
                       ('1','January'),
                       ('2','February'),
                       ('3','March'),
                       ('4','April'),
                       ('5','May'),
                       ('6','June'),
                       ('7','July'),
                       ('8','August'),
                       ('9','September'),
                       ('10','October'),
                       ('11','November'),
                       ('12','December'),
                       ]
    
    _columns = {
       'net_days': fields.function(_net_days, type="float", digits_compute=dp.get_precision('HR'), string="Total Days", method=True),
       'net_hours': fields.function(_net_hours,type="float", digits_compute=dp.get_precision('HR'), string="Total Hours", method=True),
       'total_exp': fields.function(_get_expense, type="float", digits_compute=dp.get_precision('HR'), string="Total Expenses", method=True),
       'child_exps_under_16': fields.function(_get_child_under_16_exp, digits_compute=dp.get_precision('HR'), type="float", method=True, string="Child Expenses"),
       'child_exps_over_16': fields.function(_get_child_over_or_equal_16_exp, digits_compute=dp.get_precision('HR'), type="float", method=True, string="Child Expenses"),
       'my_timesheet_id': fields.many2one('hr.my.timesheet', 'My Timesheet'),
       'input_line_ids': fields.one2many('custom.hr.payslip.input', 'payslip_id', 'Payslip Inputs', required=False, readonly=True, states={'draft': [('readonly', False)]}, copy=True),
       'my_timesheet_emp_line': fields.function(_get_timesheet_line,type="one2many",relation="hr.my.timesheet.employee",string="Timesheet Line"),
       'total_basic_salary': fields.function(_calculate_basic, method=True, type='float', string='Total', digits_compute=dp.get_precision('Payroll')),
       'month_select': fields.selection(_GET_MONTH_LIST, 'Month'),
       'disable_rule_reload':fields.boolean(string='Disable Rule Reload'),
    }
    
    _defaults = {
                 'journal_id':_get_journal,
                 'disable_rule_reload' :False,
                 }
    
    def onchange_monthlist(self, cr, uid, ids, months,context={}):
        res = {'value': {'date_from': False,'date_to': False}}
        if months:
            now = datetime.now() 
            date_from = date(now.year,int(months),1)
            date_to = date(now.year,int(months),calendar.monthrange(now.year,int(months))[1])
            res['value']['date_from'] =  date_from
            res['value']['date_to'] =  date_to
        return res 
        
    def cancel_sheet_done(self, cr, uid, ids, context=None):
        move_pool = self.pool.get('account.move')
        move_ids = []
        move_to_cancel = []
        module_check = self.pool.get('ir.module.module').search(cr, uid, [('name','=', 'hr_payroll_account'),('state','=','installed')], context=context)
        if module_check:
            for slip in self.browse(cr, uid, ids, context=context):
                if slip.move_id:
                    move_ids.append(slip.move_id.id)
                    if slip.move_id.state == 'posted':
                        move_to_cancel.append(slip.move_id.id)
            if move_to_cancel:
                move_pool.button_cancel(cr, uid, move_to_cancel, context=context)
            if move_ids:
                move_pool.unlink(cr, uid, move_ids, context=context)
        self.write(cr,uid, ids, {'state':'cancel','paid':False}, context=context)
        return True
    
    def reset_to_draft(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids,{'state':'draft'}, context=context)
        for id in ids:
            self.delete_workflow(cr, uid, [id]) # TODO is it necessary to interleave the calls?
            self.create_workflow(cr, uid, [id])
        return True      
    
    
    def onchange_employee_id(self, cr, uid, ids, date_from, date_to, employee_id=False, contract_id=False, context=None):
        res = super(hr_payslip, self).onchange_employee_id(cr, uid, [], date_from, date_to, employee_id, contract_id, context=context)
#         res['value'].pop('input_line_ids', None)
        if res['value']['contract_id']:
            my_timesheet_obj = self.pool.get('hr.my.timesheet.employee')
            contract_browse = self.pool.get('hr.contract').browse(cr, uid, res['value']['contract_id'], context=context)
            if len(contract_browse.hour_rate_line_ids) == 0 and contract_browse.month_hours <= 0.0:
                return res
            my_day_ids = my_timesheet_obj.search(cr, uid, [('my_timesheet_id.date_from', '=', date_from), ('my_timesheet_id.date_to', '=', date_to),('employee_id','=',employee_id),('my_timesheet_id.state','=','done')], context=context)
            if not my_day_ids:
                res.update({'warning': {
                       'title': _('Invalid Data!'),
                       'message': _('Please select valid period date range as per your timesheet entry...!')
                }})
            worked_days_line_ids = self.get_worked_day_lines(cr, uid, [res['value']['contract_id']] , date_from, date_to, context=context)
            input_line_ids = self.get_inputs(cr, uid, [res['value']['contract_id']], date_from, date_to, context=context)
            res['value'].update({
                'worked_days_line_ids': worked_days_line_ids,
                'date_from': date_from,
                'date_to': date_to,
                'input_line_ids': input_line_ids,
                'my_timesheet_id': my_day_ids and self.pool.get('hr.my.timesheet.employee').browse(cr, uid, my_day_ids[0], context=context).my_timesheet_id.id or False,
            })
        return res
    
    def compute_sheet(self, cr, uid, ids, context=None):
        res = []
        data_list = []
        dic = {}
        gross_dic = {}
        ded_dic = {}
        total_data_dic ={}
        gross_total = 0.0
        total = 0.0
        net_total = 0.0
        wage_total = 0.0
        payslip_obj = self.pool.get('hr.payslip')
        payslip_line_obj = self.pool.get('hr.payslip.line')
        obj_rule = self.pool.get('hr.salary.rule')
        payslip = payslip_obj.browse(cr, uid, ids, context=context)
        rule = obj_rule.browse(cr, uid, ids, context=context)
        if payslip.disable_rule_reload == False:
            result = super(hr_payslip, self).compute_sheet(cr, uid, ids, context=context)
            work_line_pool = self.pool.get('hr.payslip.worked_days')
            for payslip in self.browse(cr, uid, ids, context=context):
                old_work_ids = work_line_pool.search(cr, uid, [('payslip_id', '=', payslip.id)], context=context)
                if old_work_ids:
                    work_line_pool.unlink(cr, uid, old_work_ids, context=context)
                onchange_emp_id = self.onchange_employee_id(cr, uid, [], payslip.date_from, payslip.date_to, payslip.employee_id.id, payslip.contract_id.id, context=context)
                worked_days_line_ids = [(0,0,line) for line in onchange_emp_id['value']['worked_days_line_ids']]
                self.write(cr, uid, [payslip.id], {'worked_days_line_ids': worked_days_line_ids}, context=context)
                
            for line in payslip.line_ids:
                if line.category_id.code in('BASIC','ALW','ALWKZ'):
                    for basic_line_data in line.category_id.code:
                        dic={
                            'total': (payslip.company_id.currency_id.round((line.quantity*line.amount*line.rate)/100))
                        }
                    res.append(dic)
                    gross_total+=payslip.company_id.currency_id.round(
                        line.total)
                    payslip_line_obj.write(
                        cr,uid,line.id,dic,context=context)
 
                if line.category_id.code=='GROSS':
                    dic.update({'amount': payslip.company_id.currency_id.round(
                        line.quantity*gross_total),'total': payslip.company_id.currency_id.round(line.quantity*gross_total),'slip_id': ids[0]})
                    gross=payslip_line_obj.write(
                        cr,uid,line.id,dic,context=context)
 
                if line.category_id.code=='DED':
                    for ded_line_data in line.category_id.code:
                        ded_dic={
                            'total': payslip.company_id.currency_id.round((line.quantity*line.amount*line.rate)/100.0)
                        }
                    res.append(ded_dic)
                    total+=payslip.company_id.currency_id.round(
                        line.total)
 
                if line.category_id.code=='Total Deduction':
                    ded_dic.update({'amount': abs(
                        line.quantity*total),'total': abs(line.quantity*total),'slip_id': ids[0]})
                    payslip_line_obj.write(
                        cr,uid,line.id,ded_dic,context=context)
 
                if line.category_id.code=='NET':
                    net_total+=gross_total+total
                    payslip_line_obj.write(
                        cr,uid,line.id,{'amount': net_total,'total': net_total},context=context)
 
                if line.category_id.code=='WAGE':
                    wage_total+=gross_total+total
                    payslip_line_obj.write(
                        cr,uid,line.id,{'amount': wage_total,'total': wage_total},context=context)
                     
        else:
             if payslip.line_ids:
                 for line in payslip.line_ids:
                     data_dic ={
                         'name' : line.name,
                         'code' : line.code,
                         'category_id' : line.category_id.id,
                         'quantity' : line.quantity,
                         'rate' : line.rate,
                         'amount' : payslip.company_id.currency_id.round(line.amount),
                         'total' : payslip.company_id.currency_id.round(line.total)
                         }
                     data_list.append(data_dic)
                     if line.category_id.code in('BASIC','ALW'):
                        for basic_line_data in line.category_id.code:
                            dic = {
                               'total' : (payslip.company_id.currency_id.round((line.quantity * line.amount * line.rate)/100))
                             }
                        res.append(dic)
                        gross_total += payslip.company_id.currency_id.round(line.total)
                        payslip_line_obj.write(cr ,uid ,line.id,data_dic,context=context)
 
                     if line.category_id.code == 'GROSS':
                         data_dic.update({'amount' : payslip.company_id.currency_id.round(line.quantity * gross_total), 'total' : payslip.company_id.currency_id.round(line.quantity * gross_total),'slip_id' : ids[0]})
                         gross=payslip_line_obj.write(cr ,uid ,line.id,data_dic,context=context)
 
                     if line.category_id.code == 'DED':
                       for ded_line_data in line.category_id.code:
                           ded_dic = {
                              'total' : payslip.company_id.currency_id.round((line.quantity * line.amount * line.rate)/100.0)
                            }
                       res.append(ded_dic)
                       total += payslip.company_id.currency_id.round(line.total)
                        
                     if line.category_id.code == 'Total Deduction':
                         ded_dic.update({'amount' : abs(line.quantity * total), 'total' : abs(line.quantity * total),'slip_id' : ids[0]})
                         payslip_line_obj.write(cr ,uid ,line.id,ded_dic,context=context)
                          
                     if line.category_id.code == 'NET':
                         net_total += gross_total + total
                         payslip_line_obj.write(cr ,uid ,line.id,{'amount': net_total,'total': net_total},context=context)
                          
                     if line.category_id.code == 'WAGE':
                         wage_total += gross_total + total
                         payslip_line_obj.write(cr ,uid ,line.id,{'amount': wage_total,'total': wage_total},context=context)
        return res
    
    def float_time_convert(self, float_val):
        hours = math.floor(abs(float_val))
        mins = abs(float_val) - hours
        mins = round(mins * 60)
        if mins >= 60.0:
            hours = hours + 1
            mins = 0.0
        float_time = '%02d:%02d' % (hours,mins)
        return float_time
    
    def get_inputs(self, cr, uid, contract_id, date_from, date_to, context=None):
        res = []
        contract_obj = self.pool.get('hr.contract')
        for contract in contract_obj.browse(cr, uid, contract_id, context=context):
            if contract.data_input_line_ids:
                for input_line in contract.data_input_line_ids:
                    res.append(input_line.id)
        return res
    
    def get_worked_day_lines(self, cr, uid, contract_ids, date_from, date_to, context=None):
        """
        @param contract_ids: list of contract id
        @return: returns a list of dict containing the input that should be applied for the given contract between date_from and date_to
        """
        def was_on_leave(employee_id, datetime_day, context=None):
            res = False
            day = datetime_day.strftime("%Y-%m-%d")
            holiday_ids = self.pool.get('hr.holidays').search(cr, uid, [('state','=','validate'),('employee_id','=',employee_id),('type','=','remove'),('date_from','<=',day),('date_to','>=',day)])
            if holiday_ids:
                res = self.pool.get('hr.holidays').browse(cr, uid, holiday_ids, context=context)[0].holiday_status_id.name
            return res
          
        def working_hours(employee_id, datetime_day, context=None):
            working_hour = 0.0
            day = datetime_day.strftime("%Y-%m-%d")
            sheet_day_obj = self.pool.get('hr_timesheet_sheet.sheet.day')
            sheet_day_ids = sheet_day_obj.search(cr, uid, [('name', '=', day), ('sheet_id.employee_id', '=', employee_id)], context=context)
            if sheet_day_ids:
                working_hour = sheet_day_obj.browse(cr, uid, sheet_day_ids[0], context=context).total_attendance
            return working_hour
          
        res = []
        for contract in self.pool.get('hr.contract').browse(cr, uid, contract_ids, context=context):
#             if not contract.working_hours:
                #fill only if the contract as a class hr_payslip_input(osv.osv):working schedule linked
#                 continue
            my_timesheet_obj = self.pool.get('hr.my.timesheet.employee')
            my_day_ids = my_timesheet_obj.search(cr, uid, [('my_timesheet_id.date_from', '=', date_from), ('my_timesheet_id.date_to', '=', date_to),('employee_id','=',contract.employee_id.id),('my_timesheet_id.state','=','done')], context=context)
            total_working_hours = 0.0
            if my_day_ids:
                for pre_hour in my_timesheet_obj.browse(cr, uid, my_day_ids, context=context):
                    total_working_hours += pre_hour.present_hours
            attendances = {
                 'name': _("Normal Working Days paid at 100%"),
                 'sequence': 1,
                 'code': 'WORK100',
                 'number_of_days': 0.0,
                 'number_of_hours': 0.0,
                 'working_hours': total_working_hours,
                 'temp_working_hours': 0.0,
                 'contract_id': contract.id,
            }
            leaves = {}
            day_from = datetime.strptime(date_from,"%Y-%m-%d")
            day_to = datetime.strptime(date_to,"%Y-%m-%d")
            nb_of_days = (day_to - day_from).days + 1
            for day in range(0, nb_of_days):
                working_hours_on_day = self.pool.get('resource.calendar').working_hours_on_day(cr, uid, contract.working_hours, day_from + timedelta(days=day), context)
                working_hour = working_hours(contract.employee_id.id, day_from + timedelta(days=day), context=context)
                attendances['working_hours'] += working_hour
                if working_hours_on_day:
                    #the employee had to work
                    leave_type = was_on_leave(contract.employee_id.id, day_from + timedelta(days=day), context=context)
                    if leave_type:
                        #if he was on leave, fill the leaves dict
                        if leave_type in leaves:
                            leaves[leave_type]['number_of_days'] -= 1.0
                            leaves[leave_type]['number_of_hours'] -= working_hours_on_day
                            leaves[leave_type]['working_hours'] = 0
                        else:
                            leaves[leave_type] = {
                                'name': leave_type,
                                'sequence': 5,
                                'code': leave_type,
                                'number_of_days': -1.0,
                                'number_of_hours': -working_hours_on_day,
                                'contract_id': contract.id,
                                'working_hours': 0.0,
                            }
                    else:
                        #add the input vals to tmp (increment if existing)
                        attendances['number_of_days'] += 1.0
                        attendances['number_of_hours'] += working_hours_on_day
            hours, minutes = self.float_time_convert(attendances['working_hours']).split(":")
            if contract.month_hours > 0.0 and attendances['working_hours'] > contract.month_hours:
                attendances['working_hours'] = contract.month_hours
            attendances['temp_working_hours'] = float('%02d.%02d'%(int(hours), int(minutes)))
            overtime_rule = {} 
            if contract.month_hours > 0.0 and attendances['temp_working_hours'] > contract.month_hours:
                overtime_rule = {
                        'name': _("Overtime Hours"),
                         'sequence': 2,
                         'code': 'Overtime Hours',
                         'number_of_days': 0.0,
                         'number_of_hours': 0.0,
                         'working_hours': attendances['temp_working_hours'] - contract.month_hours,
                         'contract_id': contract.id,
                    }
            leaves = [value for key,value in leaves.items()]
            res += [attendances] 
            res += overtime_rule and [overtime_rule] or []
            res += leaves
        return res
    
    
hr_payslip()

class custom_hr_payslip_input(osv.osv):
    _name = 'custom.hr.payslip.input'
    
    _columns = {
        'name': fields.char('Description', required=True),
        'salary_rule_id':fields.many2one('hr.salary.rule',"Salary rule"),
        'payslip_id': fields.many2one('hr.payslip', 'Pay Slip', required=False),
        'code': fields.char('Code', size=52, required=True, help="The code that can be used in the salary rules"),
        'amount': fields.float('Amount'),
        'sequence': fields.integer('Sequence', required=True, select=True),
        'contract_id': fields.many2one('hr.contract', 'Contract', required=False, help="The contract for which applied this input"),
    }
    
    _defaults = {
        'amount': 0.0,
        'sequence': 10,
    }
    
class hr_payslip_line(osv.osv):
     
     _inherit = 'hr.payslip.line'
     
     def _calculate_total(self, cr, uid, ids, name, args, context):
        if not ids: return {}
        res = {}
        for line in self.browse(cr, uid, ids, context=context):
            res[line.id] = line.employee_id.company_id.currency_id.round(float(line.quantity) * line.amount * line.rate / 100)
        return res
    
     _columns = {
        'total': fields.function(_calculate_total, method=True, type='float', string='Total', digits_compute=dp.get_precision('Payroll'),store=True ),
        }
     
class hr_salary_rule(osv.osv):
    _inherit = 'hr.salary.rule'
    _order = 'sequence'
    
    def _get_dummy_sequence(self, cr, uid, ids, field_name, args, context={}):
        res = {}
        for self_obj in self.browse(cr, uid, ids, context=context):
            res[self_obj.id] = self_obj.sequence
        return res
            
    
    _columns = {
                'dummy_sequence': fields.function(_get_dummy_sequence,type='integer', string="Sequence",help='Use to arrange calculation sequence'),
                'child_age_selection':fields.selection([('under_16','Under 16'),('over_16','Over 16')],"Child Age Selection",help="Use only for child allowances!"),
                }
    
     
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: