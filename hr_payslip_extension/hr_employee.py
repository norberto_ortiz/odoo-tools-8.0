# -*- coding: utf-8 -*-
##############################################################################
#
#    Tech-Receptives Solutions Pvt. Ltd.
#    (C) Copyright 2013 Tech-Receptives Solutions (P) Ltd.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import time
from openerp.osv import fields, osv
from datetime import datetime, timedelta
import openerp.addons.decimal_precision as dp

class hr_employee(osv.osv):
    
    _inherit = "hr.employee"

    def _my_expense_count(self, cr, uid, ids, field_name, arg, context=None):
        Sheet = self.pool['hr.expense.expense']
        return { employee_id: Sheet.search_count(cr,uid, [('employee_id', '=', employee_id)], context=context) for employee_id in ids }


    _columns = {
        'ahv_nr': fields.char('AHV Nr.'),
        'child_ids': fields.one2many('hr.employee.child', 'employee_id', 'Children'),
        'my_expense_count': fields.function(_my_expense_count, type='integer', string='My Timesheets'),
        'hr_mytimesheet_line': fields.one2many('hr.my.timesheet.employee','employee_id','Timesheet',readonly=True)
    
    }
    
class hr_employee_child(osv.osv):
    
    _name = "hr.employee.child"
    
    _columns = {
        'name': fields.char('Name', required=True),
        'dob': fields.date('DOB', required=True),
        'employee_id': fields.many2one('hr.employee', 'Employee'),
    }
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: