# -*- coding: utf-8 -*-
##############################################################################
#
#    Tech-Receptives Solutions Pvt. Ltd. & Win-Soft - Web Solution
#    (C) Copyright 2013-TODAY Tech-Receptives Solutions (P) Ltd.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': 'Account Voucher Payment fix',
    'version': '1.0',
    'category': 'Account',
    'description': """
        Fix for bug: https://github.com/odoo/odoo/issues/1892

        Fix for bug: https://bugs.launchpad.net/openobject-addons/+bug/1202127
        
        Fix for bug: https://github.com/odoo/odoo/issues/6251
        
        WARNING: Don't install this module along to account_invoice_rounding!
        
        INFO: If you want special invoice rounding and correct working Register Payment process, install account_invoice_rounding!
    """,
    'author': 'Tech-Receptives Pvt. Ltd. & Win-Soft - Web Solution',
    'website': 'http://www.techreceptives.com',
    'images': [],
    'depends': ['account','account_voucher'],
    'data': [
    ],
    'installable': True,
    'auto_install': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
