# -*- coding: utf-8 -*-
#/#############################################################################
#
#    Tech-Receptives Solutions Pvt. Ltd.
#    Copyright (C) 2004-TODAY Tech-Receptives(<http://www.tech-receptives.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#/#############################################################################

import time
from openerp.osv import osv
from openerp.report import report_sxw
from openerp import pooler
from datetime import date,datetime
from openerp import netsvc
import itertools

class all_employee_payslip_report(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context={}):
        super(all_employee_payslip_report, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'time': time,
            'datetime':datetime,
            'get_data':self.get_data,
        }) 
        
    
        
    def get_data(self,data):
        payslip_browse=self.pool.get('hr.payslip')
        salary_rule_browse=self.pool.get('hr.salary.rule')
        rule_ids=salary_rule_browse.search(self.cr,self.uid,[])
        final_emp_list = []
        final_list=[]
        final_list1=[]
        res = {}
        payslip_search=payslip_browse.search(self.cr,self.uid,[('state','=','done'),('date_from','>=',data['form']['start_date']),('date_to','<=',data['form']['end_date'])])
        
        temp1 = {'month': False,'amount':0.0}
        temp_lst = []
        total = 0.0
        if payslip_search:
            for rule in payslip_browse.browse(self.cr,self.uid,payslip_search):
                date = datetime.strptime(rule.date_from, '%Y-%m-%d')
                for inv_line in rule.details_by_salary_rule_category:
                    temp_list = []
                    if temp_lst and (inv_line.salary_rule_id.id in [x['id'] for x in temp_lst]):
                        for line in temp_lst:
                            if line['id'] == inv_line.salary_rule_id.id:
                                if date.strftime('%B') in [x['month'] for x in line['line']]:
                                    for in_line in line['line']:
                                        if date.strftime('%B') == in_line['month']:
                                            if rule.credit_note:
                                                in_line[date.strftime('%B')] -= inv_line.total
                                                in_line['amount'] -= inv_line.total
                                            else:
                                                in_line[date.strftime('%B')]+=inv_line.total
                                                in_line['amount']+=inv_line.total
                                else:
                                    temp1 = {
                                        'month': date.strftime('%B'),
                                         'amount': inv_line.total,
					                     date.strftime('%B'): inv_line.total,
                                     }
                                    line['line'].append(temp1)
                    else: 
                        temp1 = {
                                'month': date.strftime('%B'),
                                 date.strftime('%B'): inv_line.total,
                                 'amount': inv_line.total
                                 }
                        
                        temp_list.append(temp1)
                        temp = {
                                'month': date.strftime('%B'),
                                'account_name': inv_line.name,
                                'number': inv_line.salary_rule_id.sequence,
                                'id': inv_line.salary_rule_id.id,
                                'line': temp_list,
                            }
                        temp_lst.append(temp)
        final_list = self.arrange_data(temp_lst)
        final_list1=sorted(final_list, key=lambda k: k['number'])
        res['lines'] = final_list1
        final_emp_list.append(res)
        return final_emp_list
    
    def arrange_data(self, final_list):
        lst = []
        for acc in final_list:
            d = {}
            total=0.0
            d.update({'account_name':acc['account_name'],'total':0.0})
            for month_line in acc['line']:
                total +=month_line['amount']
                d.update({month_line['month']: month_line['amount'],'number': acc['number']})
                d['total']=total
            lst.append(d)
        return lst

        
class report_all_employee_slip(osv.AbstractModel):
    _name = 'report.hr_payslip_extension_report.report_all_employee_slip'
    _inherit = 'report.abstract_report'
    _template = 'hr_payslip_extension_report.report_all_employee_slip'
    _wrapped_report_class = all_employee_payslip_report

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
