# -*- coding: utf-8 -*-
##############################################################################
#
#    Tech-Receptives Solutions Pvt. Ltd.
#    (C) Copyright 2013 Tech-Receptives Solutions (P) Ltd.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import time
from lxml import etree
from openerp.osv import fields, osv
from openerp.tools.float_utils import float_compare
import openerp.addons.decimal_precision as dp
from openerp.tools.translate import _
from datetime import datetime

class custom_payslip_wizard(osv.osv_memory):
    
    _name = "custom.payslip.wizard"    
    
    _columns = {
        'employee_ids': fields.many2many('hr.employee', 'employee_payslip_rel', 'payslip_id', 'employee_id', 'Employees'),
        'start_date':fields.date('Start Date',required=True),
        'end_date':fields.date('End Date',required=True),
            }
    
    def diff_month(self,d1, d2):
        return (d1.year - d2.year)*12 + d1.month - d2.month
    
    def print_report(self, cr, uid, ids,data, context=None):
        data = {'form': {}}
        data['form'].update(self.read(cr, uid, ids,['employee_ids','start_date','end_date'])[0])
        
        start_date=datetime.strptime(data['form']['start_date'],'%Y-%m-%d')
        end_date=datetime.strptime(data['form']['end_date'],'%Y-%m-%d')
        if abs(self.diff_month(start_date, end_date)) > 11:
           raise osv.except_osv(_('Error!'), _('You can not give a month range > 12.'))
        
        return {
                'type': 'ir.actions.report.xml',
                'report_name':'hr_payslip_extension_report.report_hr_payroll_slip',
                'datas': data,
                }
    
    
    
    
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
