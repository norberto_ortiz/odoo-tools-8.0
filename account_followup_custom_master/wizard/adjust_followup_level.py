# -*- coding: utf-8 -*-
##############################################################################
#
#    Tech-Receptives Solutions Pvt. Ltd.
#    Copyright (C) 2009-TODAY Tech-Receptives(<http://www.tech-receptives.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp.osv import fields,osv
import time
from datetime import datetime
from datetime import timedelta
from openerp.tools.translate import _

class adjust_followup_levels(osv.osv_memory):
    _name = "adjust.followup.level"
    _columns = {
        'followup_level_id': fields.many2one('account_followup.followup.line', 'Follow-up Levels', required=True)
    }

    _defaults = {
    }


    def button_adjust_followup_levels(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        move_line_pool = self.pool.get('account.move.line')
        fup_line_pool = self.pool.get('account_followup.followup.line')
        if context is None:
            context = {}
        data = self.read(cr, uid, ids, [], context=context)[0]
        move_line_ids = move_line_pool.search(cr, uid, [('reconcile_id', '=', False),('partner_id','=',context.get('active_id', False)), 
                            ('account_id.active','=', True), ('account_id.type', '=', 'receivable'), ('state', '!=', 'draft')])
        self.pool.get('account.move.line').write(cr, uid, move_line_ids, {'followup_line_id': data['followup_level_id'][0],'followup_date': time.strftime('%Y-%m-%d')},context=context)
        return True
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

