# -*- coding: utf-8 -*-
##############################################################################
#
#    Tech-Receptives Solutions Pvt. Ltd.
#    Copyright (C) 2009-TODAY Tech-Receptives(<http://www.tech-receptives.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import time

from openerp.osv import fields, osv
from openerp.tools.translate import _
from datetime import date

class account_followup_followup_line(osv.Model):
    _inherit = 'account_followup.followup.line'
    
    _columns = {
        'additional_fees': fields.float('Additional Fees', required=True)
    }


class res_partner(osv.osv):
    _inherit = "res.partner"
    
    
    def do_button_print_custom(self, cr, uid, ids, context=None):
        #wizard_partner_ids are ids from special view, not from res.partner
        if not ids:
            return {}
        data = {}
        data['ids'] = ids
        datas = {
             'ids': ids,
             'model': 'res.partner',
             'form': data
        }
        return self.pool['report'].get_action(cr, uid, [], 'account_followup_custom.report_account_overdue', data=datas, context=context)
    
    
    def do_button_payment_list(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        move_ids = []
        mod_obj = self.pool.get('ir.model.data')
        partner = self.browse(cr, uid, ids[0], context=context)
        if partner.unreconciled_aml_ids:
            move_ids = [m.id for m in partner.unreconciled_aml_ids]
        model_data_ids = mod_obj.search(cr, uid,[('model','=','ir.ui.view'),('name','=','account_move_line_overdue_payment_tree')], context=context)
        resource_id = mod_obj.read(cr, uid, model_data_ids, fields=['res_id'], context=context)[0]['res_id']
        return {
            'domain': "[('id','in', %s),('account_id.type','=','receivable'),('credit','=',0.0)]"%move_ids,
            'name': 'Follow-Up list of payments',
            'view_type': 'form',
            'view_mode': 'tree',
            'res_model': 'account.move.line',
            'views': [(resource_id,'tree'),(False,'form')],
            'type': 'ir.actions.act_window',
        }
        
        
    def do_partner_mail(self, cr, uid, partner_ids, context=None):
        if context is None:
            context  = {}
        today_date  = str(date.today())
        move_line_pool = self.pool.get('account.move.line')
        fup_line_pool = self.pool.get('account_followup.followup.line')
        line_ids = fup_line_pool.search(cr, uid, [], context=context)
        for partner in self.browse(cr, uid, partner_ids, context=context):
            if not partner.latest_followup_level_id:
                if partner.unreconciled_aml_ids:
                    for move_line in partner.unreconciled_aml_ids:
                        if not move_line.blocked:
                            next_followup_id = 1
                            move_line_pool.write(cr, uid, move_line.id, {'followup_line_id': next_followup_id,'followup_date': time.strftime('%Y-%m-%d')}, context=context)
            else:
                if partner.unreconciled_aml_ids:
                    for move_line in partner.unreconciled_aml_ids:
                        if move_line.blocked:
                            continue
                        elif move_line.followup_line_id and move_line.blocked == False:
                            if move_line.date >= today_date:
                                next_id = move_line.followup_line_id.id
                            else:
                                next_id = line_ids.index(move_line.followup_line_id.id) + 1
                        else:
                            next_id = 0
                        if len(line_ids) > next_id:
                            next_followup_id = line_ids[next_id]
                            move_line_pool.write(cr, uid, move_line.id, {'followup_line_id': next_followup_id,'followup_date': time.strftime('%Y-%m-%d')}, context=context)
        return super(res_partner, self).do_partner_mail(cr, uid, partner_ids, context=context)
        
        
    
    def do_partner_print(self, cr, uid, wizard_partner_ids, data, context=None):
        #wizard_partner_ids are ids from special view, not from res.partner
        res = super(res_partner, self).do_partner_print(cr, uid, wizard_partner_ids, data, context=context)
        move_line_pool = self.pool.get('account.move.line')
        fup_line_pool = self.pool.get('account_followup.followup.line')
        line_ids = fup_line_pool.search(cr, uid, [], context=context)
        for line in self.pool.get('account_followup.stat.by.partner').browse(cr, uid, wizard_partner_ids):
            if line.partner_id:
                partner = line.partner_id
                if not partner.latest_followup_level_id:
                    if partner.unreconciled_aml_ids:
                        for move_line in partner.unreconciled_aml_ids:
                            if not move_line.blocked:
                                next_followup_id = 1
                                move_line_pool.write(cr, uid, move_line.id, {'followup_line_id': next_followup_id,'followup_date': time.strftime('%Y-%m-%d')}, context=context)
                else:
                    if partner.unreconciled_aml_ids:
                        for move_line in partner.unreconciled_aml_ids:
                            if move_line.blocked:
                                continue
                            elif move_line.followup_line_id and move_line.blocked == False:
                                if move_line.date >= today_date:
                                    next_id = move_line.followup_line_id.id
                                else:
                                    next_id = line_ids.index(move_line.followup_line_id.id) + 1
                            else:
                                next_id = 0
                            if len(line_ids) > next_id:
                                next_followup_id = line_ids[next_id]
                                move_line_pool.write(cr, uid, move_line.id, {'followup_line_id': next_followup_id,'followup_date': time.strftime('%Y-%m-%d')}, context=context)
        return res 
        
        
class account_move_line(osv.osv):
    _inherit = "account.move.line"
    _order = "date_maturity ASC"

    def _get_result(self, cr, uid, ids, name, arg, context=None):
        res = {}
        for aml in self.browse(cr, uid, ids, context=context):
            if aml.currency_id:
                res[aml.id] = (aml.debit > 0 and aml.amount_currency or 0) - (aml.credit > 0 and aml.amount_currency or 0.0)
            else:
                res[aml.id] = aml.debit - aml.credit
        return res

    _columns = {
        'result':fields.function(_get_result, type='float', method=True, 
                                string="Balance") #'balance' field is not the same
    }
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
