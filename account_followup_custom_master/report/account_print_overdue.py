# -*- coding: utf-8 -*-
#/#############################################################################
#
#    Tech-Receptives Solutions Pvt. Ltd.
#    Copyright (C) 2004-TODAY Tech-Receptives(<http://www.tech-receptives.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#/#############################################################################

import time
from datetime import datetime
from openerp.report import report_sxw
from openerp import pooler
from openerp.tools.translate import _
from openerp.osv import fields, osv, orm
from datetime import date


class account_print_overdue(report_sxw.rml_parse):
    
    def __init__(self, cr, uid, name, context={}):
        super(account_print_overdue, self).__init__(cr, uid, name, context=context)
        
        self.localcontext.update({
            'time': time,
#             'datetime': datetime,
            'get_level':self.get_level,
            'set_followup_id':self.set_followup_id,
            'getLines': self._lines_get,
            'message': self._message,
            'format_fup_text': self.format_fup_text,
            'get_date':self.get_date,
            'get_total_due': self._get_total_due,
            'get_total_paid': self._get_total_paid,
            'get_extra_fees': self.get_extra_fees,
            'get_total_due_with_fees': self._get_total_due_with_fees,
            'get_mat': self.get_mat,
        })
        self.context = context
    
    def get_mat(self, partner):
        mat_amount = reduce(lambda x, y: x + (y['result']), filter(lambda x: x['date_maturity'] < time.strftime('%Y-%m-%d'), self._lines_get(partner)), 0)
        return mat_amount
                    
    
    def _get_total_due_with_fees(self, partner):
        self.total_amount_due_with_fees = 0.0
        amount = 0.0
        fees = 0.0
        total = 0.0
        moveline_obj = self.pool.get('account.move.line')
        movelines = moveline_obj.search(self.cr, self.uid,
                [('partner_id', '=', partner.id),('invoice.type', '=', 'out_invoice'),
                    ('account_id.type', 'in', ['receivable', 'payable']),
                    ('state', '<>', 'draft'), ('reconcile_id', '=', False), ('invoice.residual','!=',0.0),('blocked', '=', False)])
        for moveline in moveline_obj.browse(self.cr, self.uid, movelines):
            self.total_amount_due_with_fees += moveline.invoice.residual
        fees = self.get_extra_fees(partner)
        total = self.total_amount_due_with_fees
        if fees:
            total = self.total_amount_due_with_fees + fees
        return total
   
    def get_extra_fees(self, partner):
        additional_fees = 0.0
        moveline_obj = self.pool.get('account.move.line')
        movelines = moveline_obj.search(self.cr, self.uid,
                [('partner_id', '=', partner.id),('invoice.type', '=', 'out_invoice'),
                    ('account_id.type', 'in', ['receivable', 'payable']),
                    ('state', '<>', 'draft'), ('reconcile_id', '=', False), ('invoice.residual','!=',0.0),('blocked', '=', False)])
        if movelines:
            query_params = (tuple(movelines),)
            self.cr.execute("select max(followup_line_id) from account_move_line where id in %s",query_params)
            data = self.cr.fetchone()
            if data:
                additional_fees = self.pool.get('account_followup.followup.line').browse(self.cr, self.uid, data[0]).additional_fees
        return additional_fees 
       
    def _get_total_due(self, partner):
        self.total_amount_due = 0.0
        moveline_obj = self.pool.get('account.move.line')
        movelines = moveline_obj.search(self.cr, self.uid,
                [('partner_id', '=', partner.id),('invoice.type', '=', 'out_invoice'),
                    ('account_id.type', 'in', ['receivable', 'payable']),
                    ('state', '<>', 'draft'), ('reconcile_id', '=', False), ('invoice.residual','!=',0.0),('blocked', '=', False)])
        for moveline in moveline_obj.browse(self.cr, self.uid, movelines):
            self.total_amount_due += moveline.invoice.residual
        return self.total_amount_due
    
    def _get_total_paid(self, partner):
        self.total_amount_paid = 0.0
        moveline_obj = self.pool.get('account.move.line')
        movelines = moveline_obj.search(self.cr, self.uid,
                [('partner_id', '=', partner.id),('invoice.type', '=', 'out_invoice'),
                    ('account_id.type', 'in', ['receivable', 'payable']),
                    ('state', '<>', 'draft'), ('reconcile_id', '=', False), ('invoice.residual','!=',0.0),('blocked', '=', False)])
        for moveline in moveline_obj.browse(self.cr, self.uid, movelines):
            total_amount = 0.0
            if moveline.currency_id:
                total_amount = (moveline.debit> 0 and moveline.amount_currency) - moveline.invoice.residual or 0.0
            else:
                total_amount = moveline.debit - moveline.invoice.residual or 0.0
            self.total_amount_paid += total_amount
        return self.total_amount_paid
    
    def get_date(self, date):
        new_date = ''
        if date:
            new_date =  datetime.strptime(date, DEFAULT_SERVER_DATE_FORMAT).strftime('%d, %a %Y')
        return new_date
    
    def get_level(self, partner_id):
        context = self.context
        partner = self.pool.get('res.partner').browse(self.cr, self.uid, partner_id, context)
        if partner.latest_followup_level_id:
            return partner.latest_followup_level_id.name or ''
        else:
            return ""
    
    def set_followup_id(self, partner):
        today_date  = str(date.today())
        print ":::::::::::today_date",today_date
        move_line_pool = self.pool.get('account.move.line')
        fup_line_pool = self.pool.get('account_followup.followup.line')
        line_ids = fup_line_pool.search(self.cr, self.uid, [])
        if not partner.latest_followup_level_id:
            if partner.unreconciled_aml_ids:
                for move_line in partner.unreconciled_aml_ids:
                    if not move_line.blocked:
                        next_followup_id = 1
                        move_line_pool.write(self.cr, self.uid, move_line.id, {'followup_line_id': next_followup_id,'followup_date': time.strftime('%Y-%m-%d')})
        else:
            if partner.unreconciled_aml_ids:
                for move_line in partner.unreconciled_aml_ids:
                    if move_line.blocked:
                        continue
                    elif move_line.followup_line_id and not move_line.blocked:
                        if move_line.followup_line_id:
                            if move_line.date >= today_date:
                                next_id = move_line.followup_line_id.id
                            else:
                                next_id = line_ids.index(move_line.followup_line_id.id) + 1
                        else:
                            next_id = 0
                    else:
                        next_id = 0
                    if len(line_ids) > next_id:
                        next_followup_id = line_ids[next_id]
                        move_line_pool.write(self.cr, self.uid, move_line.id, {'followup_line_id': next_followup_id,'followup_date': time.strftime('%Y-%m-%d')})
            
        self.cr.commit()
    
    def _lines_get(self, partner):
        moveline_obj = pooler.get_pool(self.cr.dbname).get('account.move.line')
        movelines = moveline_obj.search(self.cr, self.uid,
                [('partner_id', '=', partner.id),('invoice.type', '=', 'out_invoice'),
                    ('account_id.type', 'in', ['receivable', 'payable']),
                    ('state', '<>', 'draft'), ('reconcile_id', '=', False), ('invoice.residual','!=',0.0),('blocked', '=', False)])
        movelines = moveline_obj.browse(self.cr, self.uid, movelines)
        return movelines
    
    def _message(self, obj, company):
        company_pool = pooler.get_pool(self.cr.dbname).get('res.company')
        message = company_pool.browse(self.cr, self.uid, company.id, {'lang':obj.lang}).overdue_msg
        return message.split('\n')
    
    def format_fup_text(self, o):
        context = self.context
        text = ""
        partner = self.pool.get('res.partner').browse(self.cr, self.uid, o.id, context=context)
        if partner.latest_followup_level_id:
            text = partner.latest_followup_level_id.description or ''
        if text:
            text = text % {
                'partner_name': partner.name,
                'date': time.strftime('%d.%m.%Y'),
                'company_name': partner.name,
                'user_signature': self.pool.get('res.users').browse(self.cr, self.uid, self.uid, context).signature or '',
                'currency': partner.property_product_pricelist.currency_id.name,
                'total': "%.2f" % partner.payment_amount_due
            }
        return text
    
    
class report_account_overdue(osv.AbstractModel):
    _name = 'report.account_followup_custom.report_account_overdue'
    _inherit = 'report.abstract_report'
    _template = 'account_followup_custom.report_account_overdue'
    _wrapped_report_class = account_print_overdue
    

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: