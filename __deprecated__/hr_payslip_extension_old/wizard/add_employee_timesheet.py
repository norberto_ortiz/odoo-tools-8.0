# -*- coding: utf-8 -*-
##############################################################################
#
#    Tech-Receptives Solutions Pvt. Ltd.
#    (C) Copyright 2013 Tech-Receptives Solutions (P) Ltd.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import osv, fields
from openerp.tools.translate import _
import time

class add_emp_timesheet(osv.osv_memory):
    """ Add Employees in Timesheet """

    _name = 'add.emp.timesheet'
    
    _columns = {
                'employee_ids': fields.many2many('hr.employee', 'hr_emp_timesheet_rel','timesheet_id','employee_id', 'Employees', required=True),
                'domain_field': fields.char('Domain Field'),
                }
    
    def default_get(self, cr, uid, fields, context=None):
        final_lst = []
        if context is None:
            context = {}
        res = super(add_emp_timesheet, self).default_get(cr, uid, fields, context=context)
        if 'domain_field' in fields:
            if 'active_id' in context:
                timesheet = self.pool.get(context['active_model']).browse(cr, uid, context['active_id'], context=context)
                final_lst = [x.employee_id.id for x in timesheet.timesheet_employee_line if timesheet.timesheet_employee_line]
            res.update({'domain_field': final_lst})
        return res
    
    
    def add_emp_timesheet(self, cr, uid, ids, context={}):
        self_obj = self.browse(cr, uid, ids[0], context=context)
        if 'active_ids' in context:
            for timesheet in self.pool.get(context['active_model']).browse(cr, uid, context['active_ids'], context=context):
                for emp in self_obj.employee_ids:
                    self.pool.get('hr.my.timesheet.employee').create(cr, uid, {'employee_id': emp.id, 'my_timesheet_id': timesheet.id}, context=context)
        return {'type': 'ir.actions.act_window_close'}
    