# -*- coding: utf-8 -*-
##############################################################################
#
#    Tech-Receptives Solutions Pvt. Ltd.
#    Copyright (C) 2011-TODAY Tech-Receptives(<http://www.tech-receptives.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import itertools
from lxml import etree

from openerp import models, fields, api, _
from openerp.exceptions import except_orm, Warning, RedirectWarning
from openerp.tools import float_compare
import openerp.addons.decimal_precision as dp



class account_invoice_tax(models.Model):
    _inherit = 'account.invoice.tax'
    
    @api.v8
    def compute(self, invoice):
        tax_grouped = {}
        currency = invoice.currency_id.with_context(date=invoice.date_invoice or fields.Date.context_today(invoice))
        company_currency = invoice.company_id.currency_id
        for line in invoice.invoice_line:
            taxes = line.invoice_line_tax_id.compute_all(
                (line.price_unit * (1 - (line.discount or 0.0) / 100.0)),
                line.quantity, line.product_id, invoice.partner_id)['taxes']
            inv_type_ex = [x for x in line.invoice_line_tax_id if x.manual_amount_tax == True or x.manual_amount_tax == True and x.price_include == True]
            for tax in taxes:
                if inv_type_ex:
                    val = {
                        'invoice_id': invoice.id,
                        'name': tax['name'],
                        'amount': line.manual_tax_amount,
                        'manual': False,
                        'sequence': tax['sequence'],
                        'base': line.price_unit,
                    }
                else :
                    val = {
                        'invoice_id': invoice.id,
                        'name': tax['name'],
                        'amount': tax['amount'],
                        'manual': False,
                        'sequence': tax['sequence'],
                        'base': currency.round(tax['price_unit'] * line['quantity']),
                    }
                 
                if invoice.type in ('out_invoice','in_invoice'):
                    val['base_code_id'] = tax['base_code_id']
                    val['tax_code_id'] = tax['tax_code_id']
                    val['base_amount'] = currency.compute(val['base'] * tax['base_sign'], company_currency, round=False)
                    val['tax_amount'] = currency.compute(val['amount'] * tax['tax_sign'], company_currency, round=False)
                    val['account_id'] = tax['account_collected_id'] or line.account_id.id
                    val['account_analytic_id'] = tax['account_analytic_collected_id']
                else:
                    val['base_code_id'] = tax['ref_base_code_id']
                    val['tax_code_id'] = tax['ref_tax_code_id']
                    val['base_amount'] = currency.compute(val['base'] * tax['ref_base_sign'], company_currency, round=False)
                    val['tax_amount'] = currency.compute(val['amount'] * tax['ref_tax_sign'], company_currency, round=False)
                    val['account_id'] = tax['account_paid_id'] or line.account_id.id
                    val['account_analytic_id'] = tax['account_analytic_paid_id']
 
                # If the taxes generate moves on the same financial account as the invoice line
                # and no default analytic account is defined at the tax level, propagate the
                # analytic account from the invoice line to the tax line. This is necessary
                # in situations were (part of) the taxes cannot be reclaimed,
                # to ensure the tax move is allocated to the proper analytic account.
                if not val.get('account_analytic_id') and line.account_analytic_id and val['account_id'] == line.account_id.id:
                    val['account_analytic_id'] = line.account_analytic_id.id
 
                key = (val['tax_code_id'], val['base_code_id'], val['account_id'])
                if not key in tax_grouped:
                    tax_grouped[key] = val
                else:
                    tax_grouped[key]['base'] += val['base']
                    tax_grouped[key]['amount'] += val['amount']
                    tax_grouped[key]['base_amount'] += val['base_amount']
                    tax_grouped[key]['tax_amount'] += val['tax_amount']
 
        for t in tax_grouped.values():
            t['base'] = currency.round(t['base'])
            t['amount'] = currency.round(t['amount'])
            t['base_amount'] = currency.round(t['base_amount'])
            t['tax_amount'] = currency.round(t['tax_amount'])
 
        return tax_grouped
    
    @api.model
    def move_line_get(self, invoice_id):
        res = []
        inv = self.pool.get('account.invoice').browse(self.env.cr, self.env.uid, invoice_id)
        if not inv.company_id.vat_account:
            raise except_orm(_('No Vat Account!'),
                _("You must define VAT Account for company '%s'!") % (inv.company_id.name,))
        self._cr.execute(
            'SELECT * FROM account_invoice_tax WHERE invoice_id = %s',
            (invoice_id,)
        )
        total_amt = 0.0
        total_tax = 0.0
        for row in self._cr.dictfetchall():
            if not (row['amount'] or row['tax_code_id'] or row['tax_amount']):
                continue
            res.append({
                'type': 'tax',
                'name': row['name'],
                'price_unit': row['amount'],
                'quantity': 1,
                'price': row['amount'] or 0.0,
                'account_id': inv.company_id.vat_account.id,
                'tax_code_id': row['tax_code_id'],
                'tax_amount': row['tax_amount'],
                'account_analytic_id': row['account_analytic_id'],
            })
        return res


class account_invoice(models.Model):
    _inherit = 'account.invoice'


    @api.one
    @api.depends(
        'move_id.line_id.reconcile_id.line_id',
        'move_id.line_id.reconcile_partial_id.line_partial_ids',
    )
    def _compute_payments(self):
        temp_vou = partial_lines = lines = self.env['account.move.line']
        for line in self.move_id.line_id:
            if line.account_id != self.account_id:
                continue
            if line.reconcile_id:
                lines |= line.reconcile_id.line_id
            elif line.reconcile_partial_id:
                lines |= line.reconcile_partial_id.line_partial_ids
            partial_lines += line
        temp = (lines - partial_lines).sorted()
        if temp:
            for vml in temp:
                if self.type in ('in_invoice','out_refund'):
                    vou_ml = self.env['account.move.line'].search([('move_id','=',vml.move_id.id),('tax_code_id','!=',False),('debit','>',0.0),('reconcile_id','=',False)])
                else:
                    vou_ml = self.env['account.move.line'].search([('move_id','=',vml.move_id.id),('tax_code_id','!=',False),('credit','>',0.0),('reconcile_id','=',False)])
                if vou_ml:
                    temp_vou += vou_ml
        self.payment_ids = temp.sorted()
        self.tax_payment_ids = temp_vou.sorted()
        
        
        
    tax_payment_ids = fields.Many2many('account.move.line', string='Payments',
        compute='_compute_payments')

    
    def make_register_payment(self, cr, uid, invoice_id, data, context={}):
        inv_pool = self.pool.get('account.invoice')
        voucher_pool = self.pool.get('account.voucher')
        currency_obj = self.pool.get('res.currency')
        context.update({'active_model':'account.invoice', 'active_ids': [invoice_id.id], 'active_id': invoice_id.id})
        for vou_data in data:
            reg_payment = inv_pool.invoice_pay_customer(cr, uid, [invoice_id.id], context=context)
            default_fields_voucher = voucher_pool.fields_get(cr, uid, context=context)
            
            default_data_voucher = voucher_pool.default_get(cr, uid, default_fields_voucher, context=context)
            ctx = dict(context, account_period_prefer_normal=True)
            default_data_voucher.update({'period_id': vou_data['period_id']})
            context.update(reg_payment['context'])
            default_data_voucher.update({'journal_id':vou_data['journal_id'],'partner_id': vou_data['partner_id'],'amount': vou_data['amount'], 'date': vou_data['date'],'type': context['type']})
            onchange_partner = voucher_pool.onchange_partner_id(cr, uid, [], vou_data['partner_id'], vou_data['journal_id'], vou_data['amount'], default_data_voucher['currency_id'], context['type'], default_data_voucher['date'], context=context)['value']
            onchange_amount = voucher_pool.onchange_amount(cr, uid, [], vou_data['amount'], default_data_voucher['payment_rate'], vou_data['partner_id'], vou_data['journal_id'], default_data_voucher['currency_id'], context['type'], default_data_voucher['date'], default_data_voucher['payment_rate_currency_id'], default_data_voucher['company_id'], context=context)['value']
            default_data_voucher.update(onchange_partner)   
            default_data_voucher.update(onchange_amount)
            if default_data_voucher['line_dr_ids']:
                dr_list = []
                for line in default_data_voucher['line_dr_ids']:
                    temp_list = [0,0,line]
                    dr_list.append(temp_list)
                default_data_voucher['line_dr_ids'] = dr_list
            if default_data_voucher['line_cr_ids']:
                cr_list = []
                for line in default_data_voucher['line_cr_ids']:
                    temp_list_cr = [0,0,line]
                    cr_list.append(temp_list_cr)
                default_data_voucher['line_cr_ids'] = cr_list
            onchange_line_ids = voucher_pool.onchange_line_ids(cr, uid, [], default_data_voucher['line_dr_ids'], default_data_voucher['line_cr_ids'], default_data_voucher['amount'], default_data_voucher['currency_id'], context['type'], context=None)['value']
            default_data_voucher.update(onchange_line_ids)
            default_data_voucher.update({'amount': invoice_id.type in ('out_refund','in_refund') and -vou_data['amount'] or vou_data['amount']})
            voucher_id = voucher_pool.create(cr, uid, default_data_voucher, context=context)
            voucher_pool.button_proforma_voucher(cr, uid, [voucher_id], context=context)
        return True
    
    
    def update_cancel_invoice_payment(self, cr, uid, ids, context={}):
        obj_account_voucher = self.pool.get('account.voucher.line')
        for inv in self.browse(cr, uid, ids, context=context):
            if inv.state == 'open' and not inv.payment_ids:
                voucher_search_ids = obj_account_voucher.search(cr,uid,[('move_line_id.move_id','=',inv.move_id.id)],0,None,None,None,False)
                if voucher_search_ids:
                    obj_account_voucher.unlink(cr,uid,voucher_search_ids,context)
                inv.action_cancel()
                inv.action_cancel_draft()
                self.pool.get('account.invoice').signal_workflow(cr, uid, [inv.id], 'invoice_open')
            else:
                final_data = []
                for payment in inv.payment_ids:
                    final_data.append({
                                       'partner_id' : payment.partner_id.id,
                                       'journal_id' : payment.journal_id.id,
                                       'amount' : payment.credit > 0.0 and payment.credit or payment.debit,
                                       'period_id': payment.period_id.id,
                                       'date': payment.date,
                                       })
                    vou_ids = self.pool.get('account.voucher').search(cr, uid, [('partner_id','=',payment.partner_id.id),('period_id','=',payment.period_id.id),('move_ids','in',(payment.id)),('amount','=',payment.credit > 0.0 and payment.credit or payment.debit)])
                    
                    if vou_ids:
                        self.pool.get('account.voucher').cancel_voucher(cr, uid, vou_ids, context=context)
                voucher_search_ids = obj_account_voucher.search(cr,uid,[('move_line_id.move_id','=',inv.move_id.id)],0,None,None,None,False)
                if voucher_search_ids:
                    test = obj_account_voucher.unlink(cr,uid,voucher_search_ids,context)
                inv.action_cancel()
                inv.action_cancel_draft()
                self.pool.get('account.invoice').signal_workflow(cr, uid, [inv.id], 'invoice_open')
                self.make_register_payment(cr, uid, inv, final_data, context=context)
        return True
    
class account_invoice_line(models.Model):
    _inherit = "account.invoice.line"
    _description = "Invoice Line"
    
    manual_tax_amount=fields.Float(string="Manual Tax Amount")
    
    @api.one
    @api.depends('price_unit', 'discount', 'invoice_line_tax_id', 'quantity',
        'product_id', 'invoice_id.partner_id', 'invoice_id.currency_id')
    def _compute_price(self):
        price = self.price_unit * (1 - (self.discount or 0.0) / 100.0)
        taxes = self.invoice_line_tax_id.compute_all(price, self.quantity, product=self.product_id, partner=self.invoice_id.partner_id)
        self.price_subtotal = taxes['total']
        for line in self.invoice_line_tax_id:
            if line.price_include == True and line.manual_amount_tax == True:
                self.price_subtotal = taxes['total_included']
            elif line.manual_amount_tax == True or line.price_include == True and line.manual_amount_tax == True:
                self.price_subtotal = taxes['total']
            else:
                self.price_subtotal = taxes['total']
        if self.invoice_id:
            self.price_subtotal = self.invoice_id.currency_id.round( self.price_subtotal )
