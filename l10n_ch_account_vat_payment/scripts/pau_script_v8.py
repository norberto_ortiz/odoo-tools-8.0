import xmlrpclib

from termcolor import colored
import sys

if len(sys.argv) == 4:
    url = 'localhost:'+sys.argv[1]
    db_name = sys.argv[2] # Database name
    password = sys.argv[3] # password for admin user
    print colored(url+" "+db_name+" "+password, "blue")

    server = xmlrpclib.ServerProxy("http://"+url+"/xmlrpc/object")

    inv_ids = server.execute(db_name, 1, password, 'account.invoice', 'search', [('state','in', ('open','paid'))])
    inv_to_exclude = [899,517,376]

    if inv_ids:
        for inv in inv_ids:
            print "invvvvvvV_______",inv
            if inv not in inv_to_exclude:
                server.execute(db_name, 1, password, 'account.invoice', 'update_cancel_invoice_payment', [inv])
    print "Invoice Doneeeeeeeeeeeeeeeeeeeeeeeeeeeee"


    voucher_ids = server.execute(db_name, 1, password, 'account.voucher', 'search', [('state','in', ('posted','proforma')),('type','in',('sale','purchase'))])
    voucher_to_exclude = [626,501]
    
    if voucher_ids:
        for voucher in voucher_ids:
            print "Voucherrrrrrr_______",voucher
            if voucher not in voucher_to_exclude:
                server.execute(db_name, 1, password, 'account.voucher', 'update_cancel_receipt_payment', [voucher])
    print "Sale-Purchase Receipt Doneeeeeeeeeeeeeee"

else:
    print colored("Please provide: server port, db name, admin password", "red")
    print ">> Example: python "+sys.argv[0]+" 8069 db_name password"
    print ">> Info: Escape special signs in password ;)"


#url = 'localhost:1717' # URL with port number
#db_name = 'db_thomi_dump1504' # Database name
#password = 'imm$911' # password for admin user
